#! /usr/bin/env python

import cv2
import numpy as np
import argparse
import os
from Undistortion import UndistortImages



class CaptureImages:

    undistort = UndistortImages()
    cam_1 = cv2.VideoCapture(0)
    cam_2 = cv2.VideoCapture(2)
    #cam_3 = cv2.VideoCapture(3)
    #cam_4 = cv2.VideoCapture(4)  
    cam1_savestream = 'cam1.avi'
    cam2_savestream = 'cam2.avi' 
    #cam3_stream = 'cam3.avi'
    #cam4_stream = 'cam4.avi'
    if (cam_1.isOpened()== False): 
        print("Error opening video stream or file")           
        """
    if (cam_2.isOpened()== False): 
        print("Error opening video stream or file")         
    if (cam_2.isOpened()== False): 
        print("Error opening video stream or file")           
    if (cam_3.isOpened()== False): 
        print("Error opening video stream or file")                           
        """         
    def processImages(self):
        CaptureImages.cam_1 = cv2.VideoCapture(1)
        CaptureImages.cam_2 = cv2.VideoCapture(2)
        #cam_3 = cv2.VideoCapture(3)
        #cam_4 = cv2.VideoCapture(4)               
        camera_name_back = 'backCamera'
        camera_name_front = 'frontCamera'
        camera_name_left = 'leftCamera'
        camera_name_right = 'rightCamera'
        cam_1 = cv2.VideoCapture(cam1_stream)
        cam_2 = cv2.VideoCapture(cam2_stream)
        
        # Read until videos are completed
        while (CaptureImages.cam_1.isOpened() and CaptureImages.cam_2.isOpened()):

            # Reading the images from the folders of all the cameras--just for testing
            # image1 = cv2.imread("/home/rohithm55/Documents/Ankit/New_Images_v2/f4_4/opencv_image_0.jpg")
            # image2 = cv2.imread("/home/rohithm55/Documents/Ankit/New_Images_v2/b3_2/opencv_image_0.jpg")
            # image3 = cv2.imread("/home/rohithm55/Documents/Ankit/New_Images_v2/l2_3/opencv_image_0.jpg")
            # image4 = cv2.imread("/home/rohithm55/Documents/Ankit/New_Images_v2/r1_1/opencv_image_0.jpg")
            #
            # image_undistorted1 = self.undistort.image_undistort(image1, camera_name_front)
            # image_undistorted2 = self.undistort.image_undistort(image2, camera_name_back)
            # image_undistorted3 = self.undistort.image_undistort(image3, camera_name_left)
            # image_undistorted4 = self.undistort.image_undistort(image4, camera_name_right)
            

            ret1, frame1 = self.cam_1.read()
            ret2, frame2 = self.cam_2.read()
            #ret3, frame3 = self.cam_3.read()
            #ret4, frame4 = self.cam_4.read()            
            # In case of cameras do this     ---->
            frame2_undistort = self.undistort.image_undistort(frame2, camera_name_right)
            frame3_undistort = self.undistort.image_undistort(frame3, camera_name_front)      
            #cv2.namedWindow('IMAGE3', cv2.WINDOW_NORMAL)
            #cv2.imshow("IMAGE3", frame2_undistort)

            #cv2.namedWindow('IMAGE4', cv2.WINDOW_NORMAL)
            #cv2.imshow("IMAGE4", frame3_undistort)            
            # In case of video files do this ---->      
            if (ret1 and ret2) == True:
                frame1_undistort = self.undistort.image_undistort(frame1, camera_name_left)
                frame2_undistort = self.undistort.image_undistort(frame2, camera_name_back)

               #cv2.namedWindow('IMAGE1', cv2.WINDOW_NORMAL)
                #cv2.imshow("IMAGE1", frame1_undistort)

                #cv2.namedWindow('IMAGE2', cv2.WINDOW_NORMAL)
                #cv2.imshow("IMAGE2", frame2_undistort)             
                 
                # Press Q on keyboard to  exit
                if cv2.waitKey(25) & 0xFF == ord('q'):
                    break
             
              # Break the loop
            else:                
                break            

        cv2.waitKey(0)
        CaptureImages.cam_1.release()
        CaptureImages.cam_2.release()
        #CaptureImages.cam_3.release()
        #CaptureImages.cam_4.release()
        cv2.destroyAllWindows() 

    def makeVideos(self):                            
        camera_name_left = 'leftCamera'        
        camera_name_back = 'backCamera'        
        #camera_name_front = 'frontCamera'
        #camera_name_right = 'rightCamera'
        currentFrame = 0
        fourcc = cv2.VideoWriter_fourcc(*'XVID')
        cam_1_stream = cv2.VideoWriter(CaptureImages.cam1_savestream, fourcc, 20.0, (640,480))
        #cam_2_stream = cv2.VideoWriter(CaptureImages.cam2_savestream, fourcc, 20.0, (640,480))
        #cam_3_stream = cv2.VideoWriter(CaptureImages.cam3_savestream, fourcc, 20.0, (640,480))
        #cam_4_stream = cv2.VideoWriter(CaptureImages.cam4_savestream, fourcc, 20.0, (640,480))                
        # Read until videos are completed
        while (CaptureImages.cam_1.isOpened and CaptureImages.cam_2.isOpened()):          

            ret1, frame1 = self.cam_1.read()
            #ret2, frame2 = self.cam_2.read()
            #ret2, frame2 = self.cam_2.read()
            #ret3, frame3 = self.cam_3.read()            

            if (ret1) == True:#and ret2
                frame1_undistort = self.undistort.image_undistort(frame1, camera_name_left)
                frame2_undistort = self.undistort.image_undistort(frame2, camera_name_back)
                #frame2_undistort = self.undistort.image_undistort(frame2, camera_name_right)
                #frame3_undistort = self.undistort.image_undistort(frame3, camera_name_front)                
                frame1_undistort = cv2.flip(frame1_undistort,1)                
                frame2_undistort = cv2.flip(frame2_undistort,1)
                #frame2_undistort = cv2.flip(frame2_undistort,1)
                #frame3_undistort = cv2.flip(frame3_undistort,1)                
                cam_1_stream.write(frame1_undistort)
                cam_2_stream.write(frame2_undistort)
                cam_3_stream.write(frame2_undistort)
                cam_4_stream.write(frame3_undistort)     

                """
                #View the saved frames
                cv2.namedWindow('IMAGE1', cv2.WINDOW_NORMAL)
                cv2.imshow("IMAGE1", frame0_undistort)

                cv2.namedWindow('IMAGE2', cv2.WINDOW_NORMAL)
                cv2.imshow("IMAGE2", frame1_undistort)             
                """
                currentFrame += 1 
            # Break the loop
            else:                
                break            
        # Press Q on keyboard to  exit
            if cv2.waitKey(25) & 0xFF == ord('q'):
                break

        cv2.waitKey(0)
        CaptureImages.cam_1.release()
        CaptureImages.cam_2.release()
        #CaptureImages.cam_3.release()
        #CaptureImages.cam_4.release()
        cam_1_stream.release()
        cam_2_stream.release()
        #cam2_stream.release()
        #cam3_stream.release()        
        cv2.destroyAllWindows() 



    def __init__(self):
        #self.processImages()
        self.makeVideos()


CaptureImages()


