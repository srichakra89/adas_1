### AUTHOR : V. SRI CHAKRA KUMAR.  Jan , 2019
## Initialisation 
import os
import sys
sys.path.append(os.getcwd())
import cv2
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.pylab as pb
import scipy.signal as sig
import math
from skimage import measure
from scipy.stats import mode 
np.set_printoptions(threshold=np.inf)
pb.show(block=True)


src = np.float32([[0, IMAGE_H], [1207, IMAGE_H], [0, 0], [IMAGE_W, 0]])
dst = np.float32([[569, IMAGE_H], [711, IMAGE_H], [0, 0], [IMAGE_W, 0]])
M = cv2.getPerspectiveTransform(src, dst) # The transformation matrix
Minv = cv2.getPerspectiveTransform(dst, src) # Inverse transformation
img = cv2.imread("output.bmp") # Read the test img
img1 = cv2.imread("left.jpg") # Read the test img
img2 = cv2.imread("back.jpg") # Read the test img
img3 = cv2.imread("right.jpg") # Read the test img
print img.shape
img = img[300:(300+IMAGE_H), 900:IMAGE_W] # Apply np slicing for ROI crop
warped_img = cv2.warpPerspective(img, M, (IMAGE_W, IMAGE_H)) # Image warping
gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
cv2.imshow("Before warp",img)
cv2.imshow("After warp",warped_img)

cv2.waitKey(0)