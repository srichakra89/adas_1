import cv2
import numpy as np

#from vehicleSurroundView.rectify.ocamcalib import *
#from vehicleSurroundView.bev.mappingPoints import *
from vehicleSurroundView.imaq.grabImages import grabFrontView,grabLeftView,grabRightView,grabBackView,getStreamImage,bev,getBirdsEyeView
from vehicleSurroundView.rectify.ocamcalib import *
from vehicleSurroundView.bev.mappingPoints import *
from vehicleSurroundView.bev.stitch import *


frontStream = grabFrontView('E:/My_Work/adas/video_frames/video_frames/front.avi') #cv2.CAP_DSHOW+0
leftStream = grabLeftView('E:/My_Work/adas/video_frames/video_frames/left.avi')
backStream = grabBackView('E:/My_Work/adas/video_frames/video_frames/back.avi')
#rightStream = grabRightView('E:/My_Work/adas/video_frames/video_frames/right.avi')
showStreams= getStreamImage()
stitch=getBirdsEyeView()
print "Start"
frontStream.start()
leftStream.start()
backStream.start()
#rightStream.start()
showStreams.start()
stitch.start()
print "After start"
for i in range(1000):

	cv2.imshow("frontStream",showStreams.uf)
	#cv2.imshow("leftStream",showStreams.ul)
	#cv2.imshow("rightStream",showStreams.ur)
	cv2.imshow("rearStream",showStreams.ub)			
	cv2.imshow("bev",stitch.bevdisp)
	cv2.waitKey(10)
	print "Running",i 
stitch.stop=True
frontStream.stop=True
leftStream.stop=True
#rightStream.stop=True
backStream.stop=True
showStreams.stop=True
## Start processing thread here
## Join processing thread here
#bev.join()
print "Stopping process"
stitch.join()
showStreams.join()
frontStream.join()
leftStream.join()
backStream.join()
#rightStream.join()
print "threads rejoined"
cv2.destroyAllWindows()


"""
l=cv2.imread('left.jpg')
r=cv2.imread('right.jpg')
f=cv2.imread('front.jpg')
b=cv2.imread('back.jpg')

uf = cv2.remap(f, mapx_persp_32_front, mapy_persp_32_front, cv2.INTER_LINEAR)
ul = cv2.remap(l, mapx_persp_32_left, mapy_persp_32_left, cv2.INTER_LINEAR)
ur = cv2.remap(r, mapx_persp_32_right, mapy_persp_32_right, cv2.INTER_LINEAR)
ub = cv2.remap(b, mapx_persp_32_back, mapy_persp_32_back, cv2.INTER_LINEAR)
bev,bm,bf,br,bb,bl=generateBirdsEyeView(uf,ur,ub,ul,[True,True,True,True])


cv2.imshow('OpenCV wr',cv2.resize(br.astype(np.uint8),(640,480)))
cv2.imshow('OpenCV wl', cv2.resize(bl.astype(np.uint8),(640,480)))
cv2.imshow('OpenCV wb', cv2.resize(bb.astype(np.uint8),(640,480)))
cv2.imshow('OpenCV wf', cv2.resize(bf.astype(np.uint8),(640,480)))
cv2.imshow('OpenCV wr',bm)
cv2.imshow('OpenCV wr',bev)

wl = cv2.warpPerspective(ul,caml_tform,(1556,1468))
wr = cv2.warpPerspective(ur,camr_tform,(1556,1468))
wf = cv2.warpPerspective(uf,camf_tform,(1556,1468))
wb = cv2.warpPerspective(ub,camb_tform,(1556,1468))
wl=cv2.resize(wl,(640,640))
wr=cv2.resize(wr,(640,640))
wb=cv2.resize(wb,(640,640))
wf=cv2.resize(wf,(640,640))
"""
