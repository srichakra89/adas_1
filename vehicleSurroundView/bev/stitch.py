import cv2
import numpy as np
import argparse
import os
import threading
import time
import scipy.io as sio
from imutils import rotate_bound
from vehicleSurroundView.bev.mappingPoints import *
np.set_printoptions(threshold=np.inf)
Tfov_mask =  np.zeros((BEV_HEIGHT,BEV_WIDTH,3),np.float32)	
Cfov1_mask =  np.zeros((BEV_HEIGHT,BEV_WIDTH,3),np.float32)	
Cfov2_mask =  np.zeros((BEV_HEIGHT,BEV_WIDTH,3),np.float32)	
Cfov3_mask =  np.zeros((BEV_HEIGHT,BEV_WIDTH,3),np.float32)	
Cfov4_mask =  np.zeros((BEV_HEIGHT,BEV_WIDTH,3),np.float32)	
def generateBirdsEyeView(f,r,b,l,M):
	## Front camera stream birds eye view	
	global Tfov_mask
	global Cfov4_mask
	global Cfov3_mask
	global Cfov2_mask
	global Cfov1_mask
	views_available=1
	if M[0] :	
		print f.shape, "r shape"			
		f= cv2.warpPerspective(f,camf_tform,(1556,1468))											
		print "f",f.shape						
		#f=cv2.normalize(f, None, alpha=0, beta=1, norm_type=cv2.NORM_MINMAX, dtype=cv2.CV_32F)*250				
		#self.fmask=cv2.threshold(self.f,1,255,cv2.THRESH_BINARY)		
		f[380:BEV_HEIGHT,:,:]=0
		gf=cv2.cvtColor(f,cv2.COLOR_BGR2GRAY)
		fmask=gf > 1
		f=f.astype(np.float32)
		#self.fmask=np.array(self.fmask,dtype=np.uint8)
		views_available+=1
	else : 
		fmask = np.zeros((BEV_HEIGHT,BEV_WIDTH),np.bool)
		f =  np.zeros((BEV_HEIGHT,BEV_WIDTH,3),np.float32)

	if M[1] :
		r=rotate_bound(r,90)
		print r.shape, "r shape"
		r=cv2.warpPerspective(r,camr_tform,(1556,1468))					
		#self.rmask=cv2.threshold(self.r,1,255,cv2.THRESH_BINARY)			
		r[:,0:BEV_WIDTH-480,:]=0
		gr=cv2.cvtColor(r,cv2.COLOR_BGR2GRAY)
		rmask=gr > 1			
		r=r.astype(np.float32)
		#self.rmask=np.array(self.rmask,dtype=np.uint8)
		views_available+=1				
		#r=cv2.normalize(r, None, alpha=0, beta=1, norm_type=cv2.NORM_MINMAX, dtype=cv2.CV_32F)*250
	else : 
		rmask=np.zeros((BEV_HEIGHT,BEV_WIDTH),np.bool)
		r=  np.zeros((BEV_HEIGHT,BEV_WIDTH,3),np.float32)

	if M[2] :
		print b.shape, "b shape"
		b=rotate_bound(b,180)
		b= cv2.warpPerspective(b,camb_tform,(1556,1468))	
		print b.shape, "r shape"		
		#self.bmask=cv2.threshold(self.b,1,255,cv2.THRESH_BINARY)

		b[0:BEV_HEIGHT-480,:,:]=0	
		gb=cv2.cvtColor(b,cv2.COLOR_BGR2GRAY)							
		bmask=gb > 1
		b=b.astype(np.float32)
		#self.bmask=np.array(self.bmask,dtype=np.uint8)
		#b=cv2.normalize(b, None, alpha=0, beta=1, norm_type=cv2.NORM_MINMAX, dtype=cv2.CV_32F)*250
		views_available+=1		
	else : 
		bmask = np.zeros((BEV_HEIGHT,BEV_WIDTH),bool)
		b =  np.zeros((BEV_HEIGHT,BEV_WIDTH,3),np.float32)					
	if M[3] :	
		print l.shape, "l shape"
		l=rotate_bound(l,-90)
		l= cv2.warpPerspective(l,caml_tform,(1556,1468))					
		#self.lmask=cv2.threshold(self.l,1,255,cv2.THRESH_BINARY)
		print l.shape, "l shape"
		l[:,480:BEV_WIDTH,:]=0
		gl=cv2.cvtColor(l,cv2.COLOR_BGR2GRAY)
		lmask=gl > 1
		l=l.astype(np.float32)
		#self.lmask=np.array(self.lmask,dtype=np.uint8)
		#l=cv2.normalize(l, None, alpha=0, beta=1, norm_type=cv2.NORM_MINMAX, dtype=cv2.CV_32F)*250
		views_available+=1
	else :
		lmask = np.zeros((BEV_HEIGHT,BEV_WIDTH),bool)
		l =  np.zeros((BEV_HEIGHT,BEV_WIDTH,3),np.float32)

	if np.any(M) :													
		print "A Frame is available", views_available	
		commonfov1flag = M[0] and M[3]
		commonfov2flag = M[0] and M[1]
		commonfov3flag = M[1] and M[2]
		commonfov4flag = M[2] and M[3]	
		print commonfov1flag, commonfov2flag,commonfov3flag,commonfov4flag							
		if commonfov1flag:
			commonfov1_mask=np.logical_and((fmask),(lmask))					
		else:
			commonfov1_mask=np.zeros((BEV_HEIGHT,BEV_WIDTH),bool)
		if commonfov1flag:
			commonfov2_mask=np.logical_and((fmask),(rmask))
		else:
			commonfov2_mask=np.zeros((BEV_HEIGHT,BEV_WIDTH),bool)				
		if commonfov3flag:
			commonfov3_mask=np.logical_and((bmask),(rmask))
		else:
			commonfov3_mask=np.zeros((BEV_HEIGHT,BEV_WIDTH),bool)
		if commonfov4flag:
			commonfov4_mask=np.logical_and((bmask),(lmask))								
		else:
			commonfov4_mask=np.zeros((BEV_HEIGHT,BEV_WIDTH),bool)

		print f.shape,l.shape,b.shape,r.shape
		views_available-=1
		commonfov = (commonfov1_mask+commonfov2_mask+commonfov3_mask+commonfov4_mask)
		commonfov = np.logical_or(np.logical_or(np.logical_or(commonfov1_mask,commonfov2_mask), commonfov3_mask),commonfov4_mask)		
		totalfov = np.logical_not(commonfov)
		totalfov = totalfov.astype(np.float32)
		commonfov1_mask = commonfov1_mask.astype(np.float32)
		commonfov2_mask = commonfov2_mask.astype(np.float32)
		commonfov3_mask = commonfov3_mask.astype(np.float32)
		commonfov4_mask = commonfov4_mask.astype(np.float32)
		Cfov1_mask[:,:,0] = commonfov1_mask
		Cfov1_mask[:,:,1] = commonfov1_mask
		Cfov1_mask[:,:,2] = commonfov1_mask
		Cfov2_mask[:,:,0] = commonfov2_mask
		Cfov2_mask[:,:,1] = commonfov2_mask
		Cfov2_mask[:,:,2] = commonfov2_mask
		Cfov3_mask[:,:,0] = commonfov3_mask
		Cfov3_mask[:,:,1] = commonfov3_mask
		Cfov3_mask[:,:,2] = commonfov3_mask
		Cfov4_mask[:,:,0] = commonfov4_mask
		Cfov4_mask[:,:,1] = commonfov4_mask
		Cfov4_mask[:,:,2] = commonfov4_mask		
		Tfov_mask[:,:,0] = totalfov
		Tfov_mask[:,:,1] = totalfov
		Tfov_mask[:,:,2] = totalfov	
		print "her ebefre stutchKdjugfbhk;lafkjugbh;a", views_available			
		bv_fov = np.multiply(((f+r+b+l)/views_available),Tfov_mask)
		bv_commonfov=np.multiply((f+l)/2,Cfov1_mask)+np.multiply((f+r)/2,Cfov2_mask)+np.multiply((r+b)/2,Cfov3_mask)+np.multiply((b+l)/2,Cfov4_mask)
		birdsEye = (bv_fov)+bv_commonfov
		birdsEye=birdsEye.astype(np.uint8)			
		birdsEyeView=cv2.resize(birdsEye,(640,480))
		bv_commonfov=bv_commonfov.astype(np.uint8)
		bv_commonfov=cv2.resize(bv_commonfov,(640,480))
		bv_fov=bv_fov.astype(np.uint8)
		bv_fov=cv2.resize(bv_fov,(640,480))			
		
		return birdsEyeView
	else : 
		return np.zeros((480,640),np.uint8)



class birdsEyeView(threading.Thread):	
	def __init__(self,f,r,b,l,M):
		threading.Thread.__init__(self)		
		self.f=f
		self.r=r
		self.b=b
		self.l=l		
		self.M=M
		self.fmask=np.zeros((BEV_HEIGHT,BEV_WIDTH),np.uint8)
		self.rmask=np.zeros((BEV_HEIGHT,BEV_WIDTH),np.uint8)		
		self.bmask=np.zeros((BEV_HEIGHT,BEV_WIDTH),np.uint8)		
		self.lmask=np.zeros((BEV_HEIGHT,BEV_WIDTH),np.uint8)
		self.commonfov1_mask=np.zeros((BEV_HEIGHT,BEV_WIDTH),np.uint8)
		self.commonfov2_mask=np.zeros((BEV_HEIGHT,BEV_WIDTH),np.uint8)		
		self.commonfov3_mask=np.zeros((BEV_HEIGHT,BEV_WIDTH),np.uint8)		
		self.commonfov4_mask=np.zeros((BEV_HEIGHT,BEV_WIDTH),np.uint8)		
		self.commonfov1=np.zeros((BEV_HEIGHT,BEV_WIDTH,3),np.uint8)
		self.commonfov2=np.zeros((BEV_HEIGHT,BEV_WIDTH,3),np.uint8)		
		self.commonfov3=np.zeros((BEV_HEIGHT,BEV_WIDTH,3),np.uint8)		
		self.commonfov4=np.zeros((BEV_HEIGHT,BEV_WIDTH,3),np.uint8)
		self.birdsEye=np.zeros((BEV_HEIGHT,BEV_WIDTH,3),np.float32)		
		self.birdsEyeView=np.zeros((IMG_HEIGHT,IMG_WIDTH,3),np.uint8)						
		self.stop=False
	def run(self,f,r,b,l,M):
		## Front camera stream birds eye view	
		M=self.M	
		while not self.stop :		
			print "f",self.f.dtype		
			views_available=0
			if M[0]==True :				
				self.fw= cv2.warpPerspective(self.f,camf_tform,(1556,1468))											
				print "f",self.fw.dtype						
				#f=cv2.normalize(f, None, alpha=0, beta=1, norm_type=cv2.NORM_MINMAX, dtype=cv2.CV_32F)*250				
				#self.fmask=cv2.threshold(self.f,1,255,cv2.THRESH_BINARY)
				self.fmask=self.fmask > 1
				#f=self.f.astype(np.float32)
				#self.fmask=np.array(self.fmask,dtype=np.uint8)
				views_available+=1

			if M[1]==True :
				self.r=cv2.rotate(self.r,cv2.ROTATE_90_CLOCKWISE)
				self.r=cv2.warpPerspective(self.r,camr_tform,(1556,1468))					
				#self.rmask=cv2.threshold(self.r,1,255,cv2.THRESH_BINARY)	
				self.rmask=self.rmask > 1			
				r=self.r.astype(np.float32)
				#self.rmask=np.array(self.rmask,dtype=np.uint8)
				views_available+=1				
				#r=cv2.normalize(r, None, alpha=0, beta=1, norm_type=cv2.NORM_MINMAX, dtype=cv2.CV_32F)*250

			if M[2]==True :
				self.b=cv2.rotate(self.b,cv2.ROTATE_180)	
				self.b= cv2.warpPerspective(self.b,camb_tform,(1556,1468))			
				#self.bmask=cv2.threshold(self.b,1,255,cv2.THRESH_BINARY)								
				self.bmask=self.bmask > 1
				b=self.b.astype(np.float32)
				#self.bmask=np.array(self.bmask,dtype=np.uint8)
				#b=cv2.normalize(b, None, alpha=0, beta=1, norm_type=cv2.NORM_MINMAX, dtype=cv2.CV_32F)*250
				views_available+=1							
			if M[3]==True :	
				self.l=cv2.rotate(self.l,cv2.ROTATE_90_COUNTERCLOCKWISE)
				self.l= cv2.warpPerspective(self.l,camb_tform,(1556,1468))					
				#self.lmask=cv2.threshold(self.l,1,255,cv2.THRESH_BINARY)
				self.lmask=self.lmask > 1
				l=self.l.astype(np.float32)
				#self.lmask=np.array(self.lmask,dtype=np.uint8)
				#l=cv2.normalize(l, None, alpha=0, beta=1, norm_type=cv2.NORM_MINMAX, dtype=cv2.CV_32F)*250
				views_available+=1														
			commonfov1flag = M[0] and M[3]
			commonfov2flag = M[0] and M[1]
			commonfov3flag = M[1] and M[2]
			commonfov4flag = M[2] and M[3]									
			if commonfov1flag:
				commonfov1_mask=np.logical_and((self.fmask),(self.lmask))					
			else:
				commonfov1_mask=self.commonfov1_mask
			if commonfov1flag:
				commonfov2_mask=np.logical_and((self.fmask),(self.rmask))
			else:
				commonfov2_mask=self.commonfov2_mask				
			if commonfov3flag:
				commonfov3_mask=np.logical_and((self.bmask),(self.rmask))
			else:
				commonfov3_mask=self.commonfov3_mask
			if commonfov4flag:
				commonfov4_mask=np.logical_and((self.bmask),(self.lmask))								
			else:
				commonfov4_mask=self.commonfov4_mask
			#print f.dtype,l.dtype,b.dtype,r.dtype
			"""
			commonfov = (commonfov1_mask+commonfov2_mask+commonfov3_mask+commonfov4_mask)/views_available
			totalfov = np.logical_not(commonfov)
			bv_fov = np.multiply(((f+r+b+l)/views_available),totalfov)
			bv_commonfov=np.multiply((f+l)/2,commonfov1_mask)+np.multiply((f+r)/2,commonfov2_mask)+np.multiply((r+b)/2,commonfov3_mask)+np.multiply((b+l)/2,commonfov4_mask)
			self.birdsEye = (bv_fov+bv_commonfov)
			print "her ebefre stutchKdjugfbhk;lafkjugbh;a"
			self.birdsEye=self.birdsEye.astype(np.uint8)			
			self.birdsEyeView=cv2.resize(self.birdsEye,(640,480))
			"""
 			
