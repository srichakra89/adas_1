import numpy as np
import argparse
import os
import threading
import time
from Queue import Queue
import cv2
from vehicleSurroundView.bev.mappingPoints import *
from vehicleSurroundView.rectify.ocamcalib import *
from vehicleSurroundView.bev.stitch import *
frontView = Queue(10)
leftView = Queue(10)
rightView = Queue(10)
backView = Queue(10)
uf=np.zeros((480,640,3),np.uint8)
ul=np.zeros((480,640,3),np.uint8)
ub=np.zeros((480,640,3),np.uint8)
ur=np.zeros((480,640,3),np.uint8)
bev=np.ones((480,640,3),np.uint8)*250 
M=[False,False,False,False]
class grabFrontView(threading.Thread):
    def __init__(self, ID):
        threading.Thread.__init__(self)
        self.ID=ID
        self.front_cam=cv2.VideoCapture(ID)
        self.stop = False

    def run(self):
        global frontView
        while True:
            if  self.stop:
                self.front_cam.release
                print "Getting out of front"                
                return             
            print "In front.run"                
            ret,frame=self.front_cam.read()
            frontView.put(frame)
            time.sleep(0.1)     

class grabLeftView(threading.Thread):
    def __init__(self, ID):
        threading.Thread.__init__(self)
        self.ID=ID
        self.left_cam=cv2.VideoCapture(ID)
        self.stop=False

    def run(self):

        global leftView
        while True:
            if  self.stop:
                self.left_cam.release
                print "Getting out of left"                
                return         
            print "In left.run"                
            ret,frame=self.left_cam.read()
            leftView.put(frame)
            time.sleep(0.1) 
class grabRightView(threading.Thread):
    def __init__(self, ID):
        threading.Thread.__init__(self)
        self.ID=ID
        self.right_cam=cv2.VideoCapture(ID)
        self.stop=False

    def run(self):
        global rightView
        while True:
            if  self.stop:
                self.right_cam.release
                print "Getting out of right"                
                return            
            print "In right.run"
            ret,frame=self.right_cam.read()
            rightView.put(frame)
            time.sleep(0.1)  


class grabBackView(threading.Thread):
    def __init__(self, ID):
        threading.Thread.__init__(self)
        self.ID=ID
        self.back_cam=cv2.VideoCapture(ID)
        self.stop=False

    def run(self):
        global backView        
        while True:
            if self.stop:
                self.back_cam.release
                print "Getting out of back"
                return
            ret,frame=self.back_cam.read()
            backView.put(frame)            
            time.sleep(0.1)    
            print "In back.run"


class getStreamImage(threading.Thread):

    def __init__(self):
        threading.Thread.__init__(self)
        self.stop=False
        self.uf=np.zeros((480,640),np.uint8)
        self.ul=np.zeros((480,640),np.uint8)
        self.ub=np.zeros((480,640),np.uint8)
        self.ur=np.zeros((480,640),np.uint8)
        self.bev=np.zeros((480,640),np.uint8)   
        self.M=[False,False,False,False]     
    def run(self):
        global frontView
        global leftView
        global rightView
        global backView     
        global uf
        global ur
        global ub
        global ul   
        global M                             
        while True:
            if self.stop : 
                return             
            #print "In show.run",frontView.empty()            
            if(not frontView.empty()):
                self.f=np.array(frontView.get(),dtype=np.uint8)
                self.uf = cv2.remap(self.f, mapx_persp_32_front, mapy_persp_32_front, cv2.INTER_LINEAR)
                cv2.line(self.uf, (40,480), (90,425), (0,0,255), 2)
                cv2.line(self.uf, (600,480), (550,425), (0,0,255), 2)
                cv2.line(self.uf, (90,425), (140,370), (0,255,255), 2)
                cv2.line(self.uf, (550,425), (500,370), (0,255,255), 2)
                cv2.line(self.uf, (140,370), (190,315), (0,255,0), 2)
                cv2.line(self.uf, (500,370), (450,315), (0,255,0), 2)
                # Horizontal Linies
                cv2.line(self.uf, (190,315), (450,315), (0,255,0), 2)
                cv2.line(self.uf, (140,370), (500,370), (0,255,255), 2)                                                
                cv2.line(self.uf, (90,425), (550,425), (0,0,255), 2)                    
                uf=self.uf
                print "uf"
                self.M[0]=True
            if(not rightView.empty()):
                self.r=np.array(rightView.get(),dtype=np.uint8)
                self.ur = cv2.remap(self.r, mapx_persp_32_right, mapy_persp_32_right, cv2.INTER_LINEAR)                
                ur=self.ur
                print "ur"
                self.M[1]=True
            if(not backView.empty()):
                self.b=np.array(backView.get(),dtype=np.uint8)
                self.ub = cv2.remap(self.b, mapx_persp_32_back, mapy_persp_32_back, cv2.INTER_LINEAR) 
                cv2.line(self.ub, (40,480), (90,425), (0,0,255), 2)
                cv2.line(self.ub, (600,480), (550,425), (0,0,255), 2)
                cv2.line(self.ub, (90,425), (140,370), (0,255,255), 2)
                cv2.line(self.ub, (550,425), (500,370), (0,255,255), 2)
                cv2.line(self.ub, (140,370), (190,315), (0,255,0), 2)
                cv2.line(self.ub, (500,370), (450,315), (0,255,0), 2)
                # Horizontal Linies
                cv2.line(self.ub, (190,315), (450,315), (0,255,0), 2)
                cv2.line(self.ub, (140,370), (500,370), (0,255,255), 2)                                                
                cv2.line(self.ub, (90,425), (550,425), (0,0,255), 2)                                                             
                ub=self.ub
                print "ub"
                self.M[2]=True
            if(not leftView.empty()):
                self.l=np.array(leftView.get(),dtype=np.uint8)  
                self.ul = cv2.remap(self.l, mapx_persp_32_left, mapy_persp_32_left, cv2.INTER_LINEAR)                                           
                ul=self.ul
                print "ul"
                self.M[3]=True   
            M=self.M
        print "Getting out of show"

class getBirdsEyeView(threading.Thread):
    def __init__(self):
        threading.Thread.__init__(self)
        self.stop = False
        #self.bev=np.ones((480,640),np.uint8)*250 
    def run(self):
        global uf
        global ur
        global ub
        global ul  
        global M
        global bev
        self.bev=bev
        self.bevdisp=bev
        while True:
            if  self.stop:                
                print "Getting out of front"                
                return             
            print " In BEV" 
            print "True Values are:",M
            self.bev=generateBirdsEyeView(uf,ur,ub,ul,M) 
            self.bevdisp=self.bev                            

