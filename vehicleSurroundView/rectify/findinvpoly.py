import math
import numpy as np
#from rig.type_casts import  NumpyFloatToFixConverter, NumpyFixToFloatConverter
def findinvpoly2(ss=None, radius=None, N=None):    
    theta =np.arange(-(np.pi/2)+0.009,1.2,0.01)
    #theta=np.reshape(theta,(1,len(theta)))
    r,ref = invFUN(ss, theta, radius)    
    #indices = np.where(~np.isnan(r))       
    theta = theta[np.nonzero(ref)]    
    r = r[np.nonzero(ref)]
    #print "In findinvpoly2","r",r
    #pol = np.around(np.polyfit(theta, r, N),4)
    pol = np.polyfit(theta, r, N)
    #print "In findinvpoly2","pol",N,pol
    err = np.absolute(r - np.around(np.polyval(pol, theta),4))#approximation error in pixels
    #print "In findinvpoly2","error",err
    return pol, err

def invFUN(ss=None, theta=None, radius=None):    
    
    m = np.tan(theta)    
    m = np.around(m,4)
    r = []
    ref=[]
    poly_coef = ss[::-1]
    #print "In invFUN before",poly_coef,len(m)
    poly_coef = np.around(poly_coef,4)
    #print "In invFUN after",poly_coef,len(m)
    poly_coef_tmp = poly_coef
    len_polycoef=len(poly_coef_tmp)-1
    for j in range(len(m)):#len(m)
        #print "coeff before",poly_coef_tmp[len_polycoef-1],m[j]
        poly_coef_tmp[len_polycoef-1]=np.around(poly_coef[len_polycoef-1]-m[j],4)
        #print j,"coeff after",poly_coef_tmp[len_polycoef-1]
        rhoTmp = np.roots(poly_coef_tmp)
        #print "In invFUN", "rho",rhoTmp
        res=rhoTmp[np.where((~np.iscomplex(rhoTmp)) & (rhoTmp>0) & (rhoTmp<radius))]
        if ((res.size==0) or (len(res) > 1)):
            r.append(np.nan)
            ref.append(0)
        else:
            #print "In invFUN","res",res 
            r.append(res)
            ref.append(1)
    return np.array(r),np.array(ref)

def findinvpoly(ss=None, radius=None):    
    maxerr = np.Infinity
    N = 1
    while maxerr > 0.05:        #Repeat until the reprojection error is smaller than 0.01 pixels
        N = N + 1
        pol, err= findinvpoly2(ss, radius, N)
        #print "findinvpoly",pol
        maxerr = np.amax(err)
        print "findinvpoly",N,maxerr
    #print "returning",pol,maxerr, N
    return pol, err, N


