from .findinvpoly import findinvpoly
import numpy as np
import cv2
import math
#from rig.type_casts import NumpyFloatToFixConverter, NumpyFixToFloatConverter
class cameraParameters:    
    def __init__(self,ss,xc,yc,c,d,e,width,height,pol):
        self.ss = ss
        self.xc=xc
        self.yc=yc
        self.c=c
        self.d=d
        self.e=e
        self.width=width
        self.height=height
        self.pol=pol

def world2cam_fast(M,ocam_model):
    ss = ocam_model.ss
    xc = ocam_model.xc
    yc = ocam_model.yc
    width = ocam_model.width
    height = ocam_model.height
    c = ocam_model.c
    d = ocam_model.d
    e = ocam_model.e
    pol = ocam_model.pol
    print pol
    npoints = M.shape[1]
    theta = np.zeros((1,npoints),np.float32)
    print theta.shape,npoints
    NORM = np.sqrt(M[0,:]**2 + M[1,:]**2)    
    ind0 = NORM[NORM == 0] #these are the scene points which are along the z-axis
    if len(ind0)>0:            
        NORM[int(ind0)] = 7./3 - 4./3 -1 #this will avoid division by ZERO later
    theta = (np.arctan( M[2,:]/NORM ))
    theta = np.reshape(theta,(1,len(theta)))
    print (theta.shape),(pol)
    rho = np.polyval( pol , theta ) #Distance in pixel of the reprojected points from the image center

    x = M[0,:]/NORM*rho ;
    y = M[1,:]/NORM*rho ;
    m=np.zeros((2,M.shape[1]),np.float32)
    #Add center coordinates
    m[0,:] = x*c + y*d + xc
    m[1,:] = x*e + y   + yc    
    return m
def get_color_from_imagepoints( im1, key1 ):    
    height = int(im1.shape[0])
    width  = int(im1.shape[1])

    key1 = np.round(key1)

     #Correct points which are outside image borders
    indH = np.where( (key1[:,0]<1) | (key1[:,0]>height) | (np.isnan(key1[:,0])) );
    key1[indH,0] = 1;
    key1[indH,1] = 1;
    indW = np.where( (key1[:,1]<1) | (key1[:,1]>width)  | (np.isnan(key1[:,1])));
    key1[indW,0] = 1;
    key1[indW,1] = 1;

    im1[0,0,0] = 0;
    im1[0,0,1] = 0;
    im1[0,0,2] = 0;

    RI = im1[:,:,0];
    GI = im1[:,:,1];
    BI = im1[:,:,2];
    RI=RI.astype(np.uint8)
    GI=GI.astype(np.uint8)
    BI=BI.astype(np.uint8)
    key1=key1.astype(np.int64)
    print key1   
    print type(key1.T),type(im1.shape[0:2])#(tuple(map(tuple,key1)))
    #print np.ravel_multi_index(key1.T,(479,639),order='C')
    r = RI.flat[np.ravel_multi_index(key1.T,(479,639),order='C')]
    g = GI.flat[np.ravel_multi_index(key1.T,(479,639),order='C')]    
    b = BI.flat[np.ravel_multi_index(key1.T,(479,639),order='C')]
    return RI,GI,BI
def undistort(ocam_model=None, img=None, fc=None, display=None):

    # Parameters of the new image
    Nwidth = 640#size of the final image
    Nheight = 480
    Nxc = (Nheight / 2)-1
    Nyc = (Nwidth / 2)-1
    Nz = -Nwidth / fc

    if not ocam_model.pol:
        width = ocam_model.width
        height = ocam_model.height
        #The ocam_model does not contain the inverse polynomial pol
        ocam_model.pol,_,_ = findinvpoly(ocam_model.ss, np.sqrt((width / 2) ** 2 + (height / 2) ** 2)) 
        #print "pol",ocam_model.pol
        #ocam_model.pol=np.reshape(ocam_model.pol,(1,len(ocam_model.pol)))       
     
    if len(img.shape) == 3:
        Nimg = np.zeros((Nheight, Nwidth, 3),np.float32)
    else:
        Nimg = np.zeros((Nheight, Nwidth),np.float32)
        end
    i, j = np.meshgrid(np.arange(0,Nheight-1), np.arange(0,Nwidth-1), indexing='ij')
    Nx = i - Nxc
    Ny = j - Nyc
    Nz = np.ones(Nx.shape,np.float32) * Nz            
    M = np.zeros((3,len(Nx)),np.float32)
    M=np.concatenate((Nx,Ny,Nz))
    m = world2cam_fast(M, ocam_model)
    #print "m",m
    I=np.zeros(img.shape,np.uint8)
    if len(img.shape) == 3:
        I= img[:, :, :]
        r, g, b = get_color_from_imagepoints(I, np.transpose(m))
        Nimg[:,:,0] = np.transpose(np.reshape(r,(Nwidth,Nheight)))
        Nimg[:,:,1] = np.transpose(np.reshape(g,(Nwidth,Nheight)))
        Nimg[:,:,2] = np.transpose(np.reshape(b,(Nwidth,Nheight)))
        Nimg = Nimg.astype(np.uint8)    
    return Nimg
    